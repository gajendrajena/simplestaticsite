// Ensure document is loaded before binding events on input fields
ready(function(){

  // Invalid Callback defines a set of activity, that
  // should be done when a form field is invalid
  var invalidCallbackFunction = function(field) {
    console.log('invalid callback');
    var nextElement = getNextElement(field);
    removeClass(nextElement, 'hidden'); 
    addClass(nextElement, 'visible');
    addClass(field, 'error'); 
  };
  
  // Valid Callback defines a set of activity, that
  // should be done when a form field is Valid
  var validCallbackFunction = function(field) {
    console.log('Valid callback');
    var nextElement = getNextElement(field);
    removeClass(nextElement, 'visible'); 
    addClass(nextElement, 'hidden');
    removeClass(field, 'error');
  };

  // Add events on all inputs for validation on blur.
  var fields = document.getElementsByTagName('input');
  for(i = 0; i < fields.length; i ++)
  {
    if(fields[i].getAttribute("required") || fields[i].getAttribute("pattern")) {
      addEvent(fields[i], 'blur', function(e, target)
      {
        validate(target, invalidCallbackFunction, validCallbackFunction);
      });  
    }
  }

});

  // Calculates insurance based on User selection
  function calculateInsurance(carType, age){
    // Return if any of the mandatory field is not defined
    if(carType == (undefined || null) || age == (undefined || null))
      return undefined;
    // calculate the base price, as per the spec
    var basePrice = 0;
    switch (carType){
      case "suv": 
        basePrice = 15000;
        break;
      case "small_car": 
        basePrice = 5000;
        break;
      case "big_car": 
        basePrice = 20000;
        break;
      case "people_carrier": 
        basePrice = 15000;
        break;
    }
    try {
      // If age is not a number , convert it to a number	
      if(typeof(age) != 'number')
        age = parseInt(age);
      if(isNaN(age)) throw "Age is not a number!"
      
      // If age is a number, return the insurance amount
      if(age < 30) {
        return basePrice;
      } else if (age >= 30 && age < 40) {
        return (0.8 * basePrice);
      } else if(age >= 40) {
        return (0.7 * basePrice);
      }
    } catch (ex) {
      console.log('ERROR: Exception while calculating insurance amount!');
      return undefined;
    }
  }

  // Display the insurance amount and
  // ask , if he needs to send the quotation to his email.
  function displayInsurance() {
    var carType = document.getElementById("car_type").value;
    var age = document.getElementById("age").value;
    var noticeElement = document.getElementById("notice");
    var email = document.getElementById("email").value;
    // Calculate the insurance amount
    var insuranceAmount = calculateInsurance(carType, age);
    if(insuranceAmount != undefined) {
    	var notice = "Quotation for insurance is " + insuranceAmount + "." + "\r\n Would you like to get the Quote in your mail " + email + "?";
    	noticeElement.style.display = "block";
    	addClass(noticeElement, 'success');
    	// Add Notice to the Notice Container
    	addContent(noticeElement, notice);
    	// Add anchor elements to Notice Element
    	appendElement(noticeElement, 'a', {'href': '#', 'onclick': 'showMessage(\"sendmail\");', 'id': 'sendmail'});
    	appendElement(noticeElement, 'a', {'href': '#', 'onclick': 'showMessage(\"thankyou\");', 'id': 'thankyou'});
    	appendText(document.getElementById('sendmail'),' Yes ');
    	appendText(document.getElementById('thankyou'),' No ');
    } else {
    	noticeElement.style.display = "block";
  	  addClass(noticeElement, 'error');
  	  noticeElement.firstChild.nodeValue = "Unable to calculate insurance, Please check your form data!";
    }
    return false;
  }

  // Display the message , as per the type of message
  function showMessage(type) {
    if(type == 'thankyou') {
    	var notice = "Thank you, for your interest!";
    } else if(type == 'sendmail'){
      var email = document.getElementById("email").value;
    	var notice = "The Quote will be sent to your email id " + email +".";
    }
    console.log(notice);
    var noticeElement = document.getElementById("notice");
    addContent(noticeElement, notice);
  }

  // Modal Box functionality
  function showModal() {
    var insuranceType = document.getElementById('insurance_level').value;
    var message = '';
    switch (insuranceType){
      case "basic": 
        message = "Basic Plan's benefits include 3P accident cover , medical cover of Rs. 2 L , Internet renewal facility , No vehicle inspection required";
        break;
      case "standard": 
        message = "Standard Plan's benefits include 3P accident cover , theft cover, medical cover ofRs. 2L ; Internet renewal facility, upto 30% no claim bonus";
        break;
      case "advanced": 
        message = "Advanced Plan's benefits include 3P accident cover, theft cover including for accessories, medical cover of Rs. 5 L , home visit for inspection, roadside assistance , upto 30% no claim bonus, 1 free car checkup every year";
        break;
    }
    var modalContainer = document.getElementById('modal_container');
    var modalBackground = document.getElementById('modal_background');
    var messageContainer = document.getElementById('modal_message');
    displayModalBox(modalContainer, modalBackground, messageContainer, message);
  }

  // Close Modal Box
  function closeModalBox(){
    document.getElementById('modal_container').style.display='none';
    document.getElementById('modal_background').style.display='none';
  }
