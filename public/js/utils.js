// This file contains a bunch of utility functions that can be reused
// Copyright (c) Asit Moharna <asitkmoharna@gmail.com>

  // Replaces pre and post blank spaces
  function trim(text) {
    return (text || "").replace(/^\s+|\s+$/g, "");
  }

  // Checks whether a field value matches a pattern or not
  function isMatching(fieldValue, pattern){
    return new RegExp(pattern).test(fieldValue);
  }

  // class RegularExpression 
  function classRegex(className) {
    return new RegExp('(^|\\s+)'+className+'(?:$|\\s+)', 'g') 
  }
  
  // Returns if a class is present for an element
  function hasClass(element, className) {
    var reg = classRegex(className);
    return reg.test(element.className);
  }

  // Adds a class to an element
  function addClass(element, className) {
    var reg = classRegex(className);
    if (!reg.test(element.className)){
        element.className = element.className + ' ' + className;
    }
  }

  // Remove a class from an element
  function removeClass(element, className) {
    var reg = classRegex(className);
    if (reg.test(element.className)){
        element.className = trim(element.className.replace(reg, '$1'));
    }
  }

  // Add an event listener to an element to run the callback
  function addEvent(element, eventType, callback) {
    // To work with IE < 9, use attachEvent
    if(element.addEventListener){
      element.addEventListener(eventType, function(e) {
        callback(e, e.target);
      }, false);
    } else if(element.attachEvent) {
      element.attachEvent('on' + eventType, function(e) {
        callback(e, e.srcElement);
      });
    }
  }

  // Get next html element of the given element
  function getNextElement(element) {
    var next = element.nextSibling;
    while(next && next.nodeType != 1) {
      next = next.nextSibling;
    }
    return next;
  }
  
  // Check if a form field should be validated or not.
  // If the form field has an attribute 'required' or 'pattern'
  // and the field is neither readonly or disabled, then the field 
  // should be validated.
  function validationNeeded(field)
  {
    return (
      !(field.getAttribute('readonly') || field.readonly)
      &&
      !(field.getAttribute('disabled') || field.disabled)
      &&
      (field.getAttribute('required') || field.getAttribute('pattern'))
    );
  }

  // Validate a form field
  function validate(field, invalidCallback, validCallback) {
    console.log('Validate!');
    // if validation needed for the field
    if(validationNeeded(field)) {
      // check for invalidity
      var invalid = ( (field.getAttribute('required') && !field.value) 
                  || (field.getAttribute('pattern') && field.value && !isMatching(field.value, field.getAttribute('pattern'))) );
      if(!invalid && hasClass(field, "error")) {
        validCallback(field);
        // removeClass(field, "error");
      } else if (invalid && !hasClass(field, "error")) {
        invalidCallback(field);
        // addClass(field, "error");
      }  
    }
  }
 
  // Append a dom element to an element
  function appendElement(parentElement, childElement, options) {
    var newElement = document.createElement(childElement);
    var options = options || {};
    for(var option in options) {
      newElement.setAttribute(option, options[option]);
    }
    parentElement.appendChild(newElement);
  }

  // Append a text node to the parent element
  function appendText(parentElement, textContent) {
    var textNode = document.createTextNode(textContent);
    parentElement.appendChild(textNode);
  }

  // Change inner html of an element
  function addContent(element, content) {
    element.innerHTML = content;
  }

  // Display Modal Box
  function displayModalBox(modalContainer, modalBackground, messageContainer, message){
    modalContainer.style.display='block';
    modalBackground.style.display='block';
    addContent(messageContainer, message);
  }
  

  // Javascript implementation of $(document).ready()
  // Copied from http://stackoverflow.com/questions/799981/document-ready-equivalent-without-jquery
  var ready=function(){function i(){if(r.isReady){return}try{document.documentElement.doScroll("left")}catch(e){setTimeout(i,1);return}r.ready()}function s(t){r.bindReady();var n=r.type(t);e.done(t)}var e,t,n={};n["[object Boolean]"]="boolean";n["[object Number]"]="number";n["[object String]"]="string";n["[object Function]"]="function";n["[object Array]"]="array";n["[object Date]"]="date";n["[object RegExp]"]="regexp";n["[object Object]"]="object";var r={isReady:false,readyWait:1,holdReady:function(e){if(e){r.readyWait++}else{r.ready(true)}},ready:function(t){if(t===true&&!--r.readyWait||t!==true&&!r.isReady){if(!document.body){return setTimeout(r.ready,1)}r.isReady=true;if(t!==true&&--r.readyWait>0){return}e.resolveWith(document,[r])}},bindReady:function(){if(e){return}e=r._Deferred();if(document.readyState==="complete"){return setTimeout(r.ready,1)}if(document.addEventListener){document.addEventListener("DOMContentLoaded",t,false);window.addEventListener("load",r.ready,false)}else if(document.attachEvent){document.attachEvent("onreadystatechange",t);window.attachEvent("onload",r.ready);var n=false;try{n=window.frameElement==null}catch(s){}if(document.documentElement.doScroll&&n){i()}}},_Deferred:function(){var e=[],t,n,i,s={done:function(){if(!i){var n=arguments,o,u,a,f,l;if(t){l=t;t=0}for(o=0,u=n.length;o<u;o++){a=n[o];f=r.type(a);if(f==="array"){s.done.apply(s,a)}else if(f==="function"){e.push(a)}}if(l){s.resolveWith(l[0],l[1])}}return this},resolveWith:function(r,s){if(!i&&!t&&!n){s=s||[];n=1;try{while(e[0]){e.shift().apply(r,s)}}finally{t=[r,s];n=0}}return this},resolve:function(){s.resolveWith(this,arguments);return this},isResolved:function(){return!!(n||t)},cancel:function(){i=1;e=[];return this}};return s},type:function(e){return e==null?String(e):n[Object.prototype.toString.call(e)]||"object"}};if(document.addEventListener){t=function(){document.removeEventListener("DOMContentLoaded",t,false);r.ready()}}else if(document.attachEvent){t=function(){if(document.readyState==="complete"){document.detachEvent("onreadystatechange",t);r.ready()}}}return s}()
